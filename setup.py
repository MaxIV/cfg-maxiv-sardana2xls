#!/usr/bin/env python

from setuptools import setup, find_packages

setup(name = "python36-sardana2xls",
      version = "1.2.0",
      description = ("Tool to generate a xls representation of an existing Tango Sardana environment"),
      author = "KITS - Controls",
      author_mail = "kits@maxiv.lu.se",
      license = "GPLv3",
      url = "https://gitlab.maxiv.lu.se/kits-maxiv/cfg-maxiv-sardana2xls",
      packages = find_packages(),
      python_requires='~=3.6',
      include_package_data=True,
      package_data={'': ['*.xls']},
      entry_points={
          "console_scripts": ["sardana2xls = sardana2xls.sardana2xls:main"]},
      install_requires=["setuptools", "pytango", "xlrd"],
      )
