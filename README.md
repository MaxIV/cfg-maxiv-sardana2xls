# tango-sardana2xls

Tool to generate a xls representation of an existing [Tango](https://github.com/tango-controls/pytango.git) [Sardana](https://github.com/sardana-org/sardana) environment

## Dependencies

This tool requires `xlrd` and `xlutils` to run.

Note: the `xlrd` in the yum repos is quite old, and `xlutils` isn't available at all. Use pip to install them!

```sh
pip install xlrd --user
pip install xlutils --user
```

## Usage

With the current tango host
```bash
sardana2xls {Pool instance name}
```

With a different tango host
```bash
TANGO_HOST=your_tango_db_host:your_tango_db_port sardana2xls {Pool instance name}
```

## Todo
 - Requierment
 - CLI
 - Test
 
 
